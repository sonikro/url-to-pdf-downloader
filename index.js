var fetch = require('node-fetch')
const fs = require(`fs`);

const urlDatas = require("./parameters.json");

console.log(`Running URL to PDF Downloader:`)
console.log(`URLs to Download: ${urlDatas}`)


async function downloadUrl(urlData) {

    const pdfLocation = `./${urlData.filename}`
    const file = await fs.existsSync(pdfLocation)
    if(file){
        console.log(`Skipping ${pdfLocation} because it already exists.`);
        return;
    }
    console.log(`Downloading URL ${urlData.url} to file ${urlData.filename}`)
    const res = await fetch(`http://localhost:9000/api/render?url=${urlData.url}`);
    await new Promise((resolve, reject) => {
        const fileStream = fs.createWriteStream(pdfLocation);
        res.body.pipe(fileStream);
        res.body.on("error", (err) => {
          reject(err);
        });
        fileStream.on("finish", function() {
          resolve();
        });
      });

}

async function runProgram() {
    for (var urlData of urlDatas) {
         await downloadUrl(urlData);
    }
}

runProgram().then(
    (success) => {  }
)